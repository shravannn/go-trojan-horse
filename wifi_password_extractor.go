package main

import (
	"fmt"
	"os/exec"
	"regexp"
	"strings"
)

func stringInString(s string, S string) (bool, error) {
	match1, err := regexp.MatchString(s, S)
	// fmt.Println("Match: ", match1, " Error: ", err)
	return match1, err
}

func getProfiles() ([]string, error) {
	// * extracting all wifi profiles
	cmd := exec.Command("netsh", "wlan", "show", "profiles")
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return []string{}, err
	}

	profiles := []string{}
	output := strings.Split(string(stdout), "\n")

	// * decoding the output
	for _, line := range output {
		match, _ := stringInString("All User Profile", line)
		if match {
			profName := strings.TrimSpace(strings.Split(line, ":")[len(strings.Split(line, ":"))-1])
			// fmt.Println("Profile:", profName)
			profiles = append(profiles, profName)
		}
	}

	return profiles, nil
}

func getPasswords(profiles []string) (map[string]string, error) {
	wifiPasswords := map[string]string{}

	// * extracting passwords from profiles
	for _, prof := range profiles {
		cmd := exec.Command("netsh", "wlan", "show", "profile", prof, "key=clear")
		stdout, err := cmd.Output()

		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		// * decoding the output
		output := strings.Split(string(stdout), "\n")
		for _, line := range output {
			match, _ := stringInString("Key Content", line)
			if match {
				password := strings.TrimSpace(strings.Split(line, ":")[len(strings.Split(line, ":"))-1])
				// fmt.Println("Password:", password)
				wifiPasswords[prof] = password
			}
		}
	}

	return wifiPasswords, nil
}
