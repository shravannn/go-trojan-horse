package main

import (
	"fmt"
	"math/rand"
	"time"
)

func game() {
	r := rand.Intn(500)
	fmt.Println("Welcome to the Go guess the number game!")
	fmt.Println("The number is between 1 and 1000.")

	rand.Seed(time.Now().UnixNano())
	n := r + rand.Intn(700-r+1)

	for {
		var guess int
		fmt.Print("Guess the number: ")
		fmt.Scanln(&guess)

		if guess > n {
			fmt.Println("Decrease the number!")
		} else if guess < n {
			fmt.Println("Increase the number!")
		} else {
			fmt.Println("You won the game!")
			break
		}

		fmt.Println("\n---------------------------------------")
	}
}
